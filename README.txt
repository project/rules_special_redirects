Rules Special Redirects
=======================

Synopsis
-------

Tiny helper module that allows rules actions to trigger Page Not Found and 
Access Denied pages as Rules actions.

Details
-------

This module enables rules to redirect to special pages as part of rules actions.

Currently, you can redirect to:
* Page Not Found
* Access Denied

How to use
----------

1.  Install and enable this module.
2.  Create a new Rule, and set events and conditions.
3.  Under Actions, select one of the rules under the Rules Special Redirects
    group.
4.  If you're performing any other actions, make sure it's the last one in the 
    list, otherwise the others won't trigger.
5.  Save your rule.

Now whenever your rule is triggered, you will be redirected to the special page.

Authors
-------

* Geoffrey Roberts (geoffreyr)




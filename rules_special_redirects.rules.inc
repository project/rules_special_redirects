<?php

/**
 * Implements hook_rules_action_info().
 */
function rules_special_redirects_rules_action_info() {
  return array(
    'rules_special_redirects_not_found' => array(
      'label' => t('Redirect to Page Not Found'), 
      'parameter' => array(), 
      'group' => t('Rules Special Redirects'), 
      'callbacks' => array(),
    ),
    'rules_special_redirects_access_denied' => array(
      'label' => t('Redirect to Access Denied'), 
      'parameter' => array(), 
      'group' => t('Rules Special Redirects'), 
      'callbacks' => array(),
    ),
  );
}

/**
 * Triggers drupal_not_found() as a rule action.
 */
function rules_special_redirects_not_found() {
  drupal_not_found();
  drupal_exit();
}

/**
 * Triggers drupal_access_denied() as a rule action.
 */
function rules_special_redirects_access_denied() {
  drupal_access_denied();
  drupal_exit();
}
